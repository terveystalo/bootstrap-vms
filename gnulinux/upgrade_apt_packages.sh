#!/usr/bin/env bash

# Forked from:
# https://raw.githubusercontent.com/geekzter/bootstrap-os/master/linux/bootstrap_linux2.sh

SCRIPT_PATH=$(dirname "$0")

# Detect Linux distribution
if test ! "$(command -v lsb_release)"; then
    if test "$(command -v apt-get)"; then
        # Debian/Ubuntu
        sudo apt-get install lsb-release -y
    elif test "$(command -v yum)"; then
        # CentOS/Red Hat
        sudo yum install redhat-lsb-core -y
    elif test "$(command -v zypper)"; then
        # (Open)SUSE
        sudo zypper install lsb-release -y
    else
        echo $'\nlsb_release not found, not able to detect distribution'
        exit 1
    fi
fi
if test "$(command -v lsb_release)"; then
    DISTRIB_ID=$(lsb_release -i -s)
    DISTRIB_RELEASE=$(lsb_release -r -s)
    DISTRIB_RELEASE_MAJOR=$(lsb_release -s -r | cut -d '.' -f 1)
    lsb_release -a
fi

# Packages
if test ! "$(command -v sudo)"; then
    echo $'\nsudo not found, skipping packages'
else
    if test ! "$(command -v apt-get)"; then
        echo $'\napt-get not found, skipping packages'
    else
        # pre-requisites
        sudo apt-get install -y apt-transport-https curl

        # enable Universe repository
        sudo add-apt-repository universe

        # enable Kubernetes repository
        curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
        cat <<EOF | sudo tee /etc/apt/sources.list.d/kubernetes.list
deb https://apt.kubernetes.io/ kubernetes-xenial main
EOF
        # Installs Azure CLI including dependencies
        curl -sL https://aka.ms/InstallAzureCLIDeb | sudo bash

        # Microsoft dependencies
        # Source: https://github.com/Azure/azure-functions-core-tools
        curl https://packages.microsoft.com/keys/microsoft.asc | sudo apt-key add -
        if [ "$DISTRIB_ID" == "Debian" ]; then
            wget -qO- https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > microsoft.asc.gpg
            sudo mv microsoft.asc.gpg /etc/apt/trusted.gpg.d/
            wget -q "https://packages.microsoft.com/config/debian/${DISTRIB_RELEASE_MAJOR}/prod.list"
            sudo mv prod.list /etc/apt/sources.list.d/microsoft-prod.list
            sudo chown root:root /etc/apt/trusted.gpg.d/microsoft.asc.gpg
            sudo chown root:root /etc/apt/sources.list.d/microsoft-prod.list
        fi
        if [ "$DISTRIB_ID" == "Ubuntu" ]; then
            curl "https://packages.microsoft.com/config/ubuntu/${DISTRIB_RELEASE}/prod.list" | sudo tee /etc/apt/sources.list.d/msprod.list
            sudo dpkg -i packages-microsoft-prod.deb
        fi

        echo $'\nUpdating package list...'
        sudo apt-get update

        echo $'\nUpgrading packages...'
        sudo ACCEPT_EULA=Y apt-get upgrade -y

        echo $'\nInstalling new packages...'
        INSTALLED_PACKAGES=$(mktemp)
        NEW_PACKAGES=$(mktemp)
        dpkg -l | grep ^ii | awk '{print $2}' >"$INSTALLED_PACKAGES"
        grep -Fvx -f "$INSTALLED_PACKAGES" "${SCRIPT_PATH}/apt-packages.txt" >"$NEW_PACKAGES"
        while read -r package; do
            sudo ACCEPT_EULA=Y apt-get install -y "$package"
        done < "$NEW_PACKAGES"
        rm "$INSTALLED_PACKAGES" "$NEW_PACKAGES"
    fi
fi
